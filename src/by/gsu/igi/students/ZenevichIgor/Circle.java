package by.gsu.igi.students.ZenevichIgor;

import java.util.Scanner;

class Circle {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Введите радиус окружности: ");

        double radius = in.nextDouble();

        System.out.println("Радиус=" + radius);

        double pi = 3.1416;

        double perimeter = (radius + radius) * pi;

        System.out.println("Периметр=" + perimeter);

        double area = (radius * radius) * pi;

        System.out.println("Площадь=" + area);
    }
}


package by.gsu.igi.lectures.lecture01;

/**
 * Created by Evgeniy Myslovets.
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello, " + args[0]);
    }
}
